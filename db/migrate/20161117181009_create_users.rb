class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :first_name
      t.string :vulgo
      t.string :abteilung
      t.string :uid
      t.boolean :state, default: false

      t.timestamps
    end
    add_attachment :users, :avatar
  end
end
