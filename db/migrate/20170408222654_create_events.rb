class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :state
      t.belongs_to :user

      t.timestamps
    end
  end
end
