class EventsController < ApplicationController
  http_basic_authenticate_with name: "leiter", password: "pio18", only: [:new, :create, :index]
  def index
    @events = Event.all
  end

  def add_event
    event = event_params
    Event.create! {event}
    redirect_to "/user/#{@user.uid}"
  end

  private

  def event_params
    params.require(:type).require(:user)
  end
end
