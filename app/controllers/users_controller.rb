class UsersController < ApplicationController
  http_basic_authenticate_with name: "leiter", password: "pio18", except: :show

  def show
    @show = true
    set_user
    @user.state = !@user.state
    @user.save
    state = @user.state ? 'check_in' : 'check_out'
    Event.create!(state: state, user: @user)
  end

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        Event.create!(state: 'created', user: @user)
        format.html {redirect_to new_user_path, notice: "#{@user.vulgo} erfolgreich erstellt."}
      else
        format.html {render :new}
      end
    end
  end

  def edit
    set_user
  end

  def update
    User.find(params[:uid]).update(user_params)
    redirect_to users_path
  end

  def destroy
    User.find(params[:uid]).destroy
    redirect_to users_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find_by(uid: params[:uid])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:name, :first_name, :vulgo, :abteilung, :avatar,
                                 :uid)
  end
end
