class User < ApplicationRecord
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/, message: 'Ungültiges Foto Format. Das Foto muss .jpg oder .png sein.'
  validates_with AttachmentSizeValidator, attributes: :avatar, less_than: 100.megabytes

  validates_presence_of :name
  validates_presence_of :first_name
  validates_presence_of :vulgo
  validates_presence_of :abteilung

  before_create :set_uid

  private

  def set_uid
    self.uid = SecureRandom.uuid
  end
end
