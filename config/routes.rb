Rails.application.routes.draw do
  root 'users#index'
  resources :users, param: :uid
  resources :events, only: [:index]
end
